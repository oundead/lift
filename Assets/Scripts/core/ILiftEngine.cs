﻿public interface ILiftEngine {
    IFloorListModel Floors { get; }
    ILiftButtonsModel LiftButtons { get; }
    ILiftModel Lift { get; }

    void Update (float timeDelta);
}
