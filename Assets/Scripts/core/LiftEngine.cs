﻿using System.Collections.Generic;
using UnityEngine;

public class LiftEngine : ILiftEngine {

    private bool Arrived = false;
    private readonly SortedSet<int> SchedulledFloors = new SortedSet<int> ();

    public IFloorListModel Floors { get; }
    public ILiftButtonsModel LiftButtons { get; }
    public ILiftModel Lift { get; }

    public LiftEngine (int floors, float liftAcceleration, float liftMaxSpeed) {
        Lift = new LiftModel (liftAcceleration, liftMaxSpeed, Time.maximumDeltaTime);
        Floors = new FloorListModel (floors);
        LiftButtons = new LiftButtonsModel (floors);
        Lift.AddArrivedListener (() => Arrived = true);
    }

    public void Update (float timeDelta) {
        Lift.UpdatePosition (timeDelta);
        Schedule ();
    }

    void Schedule () {
        UpdateCurrentFloorButtons ();
        ProcessArrived ();
        foreach (var f in Floors.Floors) {
            if (f.DoorState != DoorStates.CLOSED) {
                return;
            }
        }
        ScheduleDirection ();
        ValidateDirection ();
        ScheduleNextFloor ();
    }

    void ProcessArrived () {
        if (!Arrived) {
            return;
        }
        Arrived = false;
        var pos = Mathf.RoundToInt (Lift.Position);
        if (LiftButtons.Floors [pos].Toggled) {
            LiftButtons.Floors [pos].Toggled = false;
        }

        for (int i = 0; i < 2; i++) {
            if (Floors.Floors [pos].Up.Toggled &&
                (Lift.Direction == Directions.NONE || Lift.Direction == Directions.UP)) {
                Floors.Floors [pos].Up.Toggled = false;
                Lift.Direction = Directions.UP;
            }
            if (Floors.Floors [pos].Down.Toggled &&
                (Lift.Direction == Directions.NONE || Lift.Direction == Directions.DOWN)) {
                Floors.Floors [pos].Down.Toggled = false;
                Lift.Direction = Directions.DOWN;
            }
            ValidateDirection ();
        }
    }

    void UpdateCurrentFloorButtons () {
        bool arrived = false;
        if (Lift.StayingOnFloor) {
            var pos = Mathf.RoundToInt (Lift.Position);
            if (LiftButtons.Floors [pos].Toggled) {
                arrived = true;
            }
            var floor = Floors.Floors [pos];
            if (floor.Down.Toggled && Lift.Direction == Directions.DOWN ||
                floor.Up.Toggled && Lift.Direction == Directions.UP ||
                (floor.Down.Toggled || floor.Up.Toggled) && Lift.Direction == Directions.NONE) {
                arrived = true;
            }
        }
        if (arrived) {
            Lift.FireArrived ();
        }
    }

    void ScheduleDirection () {
        var pos = Mathf.RoundToInt (Lift.Position);

        if (Lift.Direction == Directions.NONE) {

            for (var i = 0; i < LiftButtons.Floors.Count; i++) {
                var f = LiftButtons.Floors [i];
                if (f.Toggled && pos != i) {
                    Lift.Direction = pos > i ? Directions.DOWN : Directions.UP;
                    return;
                }
            }

            for (var i = 0; i < Floors.Floors.Count; i++) {
                var f = Floors.Floors [i];
                if ((f.Down.Toggled || f.Up.Toggled) && i != pos) {
                    Lift.Direction = pos > i ? Directions.DOWN : Directions.UP;
                    return;
                }
            }
        }

    }

    private void ValidateDirection () {
        var pos = Mathf.RoundToInt (Lift.Position);
        if (Lift.StayingOnFloor) {
            if (Lift.Direction == Directions.UP) {
                for (var i = pos + 1; i < LiftButtons.Floors.Count; i++) {
                    var f = LiftButtons.Floors [i];
                    if (f.Toggled) {
                        return;
                    }
                }
                for (var i = pos + 1; i < LiftButtons.Floors.Count; i++) {
                    var f = Floors.Floors [i];
                    if (f.Down.Toggled || f.Up.Toggled) {
                        return;
                    }
                }
                Lift.Direction = Directions.NONE;
            } else if (Lift.Direction == Directions.DOWN) {
                for (var i = pos - 1; i >= 0; i--) {
                    var f = LiftButtons.Floors [i];
                    if (f.Toggled) {
                        return;
                    }
                }
                for (var i = pos - 1; i >= 0; i--) {
                    var f = Floors.Floors [i];
                    if (f.Down.Toggled || f.Up.Toggled) {
                        return;
                    }
                }
                Lift.Direction = Directions.NONE;
            }
        }
    }

    void ScheduleNextFloor () {
        SchedulledFloors.Clear ();
        var pos = Mathf.RoundToInt (Lift.Position);

        if (Lift.Direction == Directions.UP) {
            int? maxFloorDown = null;
            for (var i = pos + 1; i < Floors.Floors.Count; i++) {
                var floor = Floors.Floors [i];
                if (floor.Up.Toggled) {
                    SchedulledFloors.Add (i);
                }
                if (floor.Down.Toggled) {
                    maxFloorDown = i;
                }
                var liftButton = LiftButtons.Floors [i];
                if (liftButton.Toggled) {
                    SchedulledFloors.Add (i);
                }
            }
            if (maxFloorDown != null && (SchedulledFloors.Count < 1 || maxFloorDown > SchedulledFloors.Max)) {
                SchedulledFloors.Add (maxFloorDown.Value);
            }
            foreach (var i in SchedulledFloors) {
                if (Lift.TryToSetNextPosition (i)) {
                    return;
                }
            }
        } else if (Lift.Direction == Directions.DOWN) {
            int? minFloorUp = pos;
            for (var i = pos - 1; i >= 0; i--) {
                var floor = Floors.Floors [i];
                if (floor.Down.Toggled) {
                    SchedulledFloors.Add (i);
                }
                if (floor.Up.Toggled) {
                    minFloorUp = i;
                }
                var liftButton = LiftButtons.Floors [i];
                if (liftButton.Toggled) {
                    SchedulledFloors.Add (i);
                }
            }
            if (minFloorUp != null && (SchedulledFloors.Count < 1 || minFloorUp < SchedulledFloors.Min)) {
                SchedulledFloors.Add (minFloorUp.Value);
            }
            foreach (var i in SchedulledFloors.Reverse ()) {
                if (Lift.TryToSetNextPosition (i)) {
                    return;
                }
            }
        }
    }
}
