﻿public interface IFloorModel {
    DoorStates DoorState { get; set; }
    ILiftButtonModel Up { get; }
    ILiftButtonModel Down { get; }
    void AddListener (System.Action listener);
}
