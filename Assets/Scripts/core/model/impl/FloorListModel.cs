﻿using System;
using System.Collections.Generic;
using System.Linq;

public class FloorListModel : IFloorListModel {
    public IReadOnlyList<IFloorModel> Floors { get; }

    private event Action Changed;

    public FloorListModel(int floors) {
        Floors = Enumerable.Range (0, floors)
           .Select (n => new FloorModel ())
           .ToList ();
        foreach(var f in Floors) {
            f.AddListener (() => Changed?.Invoke ());
        }
    }

    public void AddListener (Action listener) {
        Changed += listener;
    }
}

