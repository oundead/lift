﻿using System;

public class LiftButtonModel : ILiftButtonModel {
    private bool _toggled = false;
    private event Action Changed;

    public bool Toggled {
        get {
            return _toggled;
        }
        set {
            if (_toggled == value) {
                return;
            }
            _toggled = value;
            Changed?.Invoke ();
        }
    }

    public void AddListener (Action listener) {
        Changed += listener;
    }
}
