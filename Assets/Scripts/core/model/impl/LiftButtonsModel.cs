﻿using System.Collections.Generic;
using System.Linq;

public class LiftButtonsModel : ILiftButtonsModel {

    public IReadOnlyList<ILiftButtonModel> Floors { get; }

    public LiftButtonsModel (int floors) {
        Floors = Enumerable.Range (0, floors)
            .Select (n => new LiftButtonModel ())
            .ToList ();
    }
}
