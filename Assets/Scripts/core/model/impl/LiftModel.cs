﻿using System;
using UnityEngine;

public class LiftModel : ILiftModel {
    private readonly float MaxDt;
    private readonly float Acceleration;
    private readonly float MaxSpeed;

    private event Action DirectionChanged;
    private event Action Arrived;

    private Directions _direction = Directions.NONE;
    private float Speed = 0;

    public float Position { get; private set; } = 0;
    public int? NextPosition { get; private set; } = null;
    public bool StayingOnFloor { get; private set; } = true;
    public ILiftButtonModel Stopped { get; }

    public Directions Direction {
        get {
            return _direction;
        }
        set {
            if (_direction == value) {
                return;
            }
            _direction = value;
            DirectionChanged?.Invoke ();
        }
    }

    public LiftModel (float acceleration, float maxSpeed, float maxDt) {
        Acceleration = acceleration;
        MaxSpeed = maxSpeed;
        Stopped = new LiftButtonModel ();
        MaxDt = maxDt * 2;
    }

    public void AddDirectionChangeListener (Action listener) {
        DirectionChanged += listener;
    }

    public bool TryToSetNextPosition (int nextPosition) {
        var posInt = Mathf.RoundToInt (Position);
        if (NextPosition == null || NextPosition.Value == nextPosition) {
            NextPosition = nextPosition;
            StayingOnFloor = false;
            return true;
        }
        if ((NextPosition > posInt && nextPosition > posInt && nextPosition < NextPosition)
            || (NextPosition < posInt && nextPosition < posInt && nextPosition > NextPosition)) {

            var nextDist = Math.Abs (Position - nextPosition);
            var stopTime = Speed / Acceleration;
            var stopDist = Math.Abs ((Speed * stopTime) / 2);

            if (nextDist > stopDist) {
                NextPosition = nextPosition;
                StayingOnFloor = false;
                return true;
            }
        }

        return false;
    }

    public void UpdatePosition (float timeDelta) {
        if (NextPosition == null) {
            return;
        }

        float inverseFactor = Position > NextPosition ? -1 : 1;

        float pos = inverseFactor * Position;
        float speed = inverseFactor * Speed;
        float nextPos = inverseFactor * NextPosition.Value;

        float dist = nextPos - pos;
        float stopTime = speed / Acceleration;
        float stopDist = speed * stopTime / 2;

        if (Stopped.Toggled) {
            speed = Math.Sign (speed) * Math.Max (Math.Abs (speed) - Acceleration * timeDelta, 0);
        } else if (speed < 0 || stopDist < dist) {
            speed = Math.Min (speed + Acceleration * timeDelta, MaxSpeed);
        } else {
            speed = Math.Max (speed - Acceleration * timeDelta, 0);
        }

        bool arrived = (Math.Abs(speed) < MaxDt * Acceleration && Math.Abs(dist) < MaxDt * MaxSpeed);
        pos = arrived ? (nextPos) : (pos + speed * timeDelta);

        Position = inverseFactor * pos;
        Speed = inverseFactor * speed;

        if (arrived) {
            NextPosition = null;
            StayingOnFloor = true;
            Arrived?.Invoke ();
        }

        // Debug.Log ("move " + _position + " " + Speed);
    }

    public void AddArrivedListener (Action listener) {
        Arrived += listener;
    }

    public void FireArrived () {
        Arrived?.Invoke ();
    }
}
