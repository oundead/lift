﻿public class FloorModel : IFloorModel {
    private event System.Action Changed;
    private DoorStates _doorState = DoorStates.CLOSED;

    public ILiftButtonModel Up { get; }
    public ILiftButtonModel Down { get; }
    public DoorStates DoorState {
        get {
            return _doorState;
        }
        set {
            if (_doorState == value) {
                return;
            }
            _doorState = value;
            Changed?.Invoke ();
        }
    }

    public FloorModel () {
        Up = new LiftButtonModel ();
        Down = new LiftButtonModel ();

        Up.AddListener (() => Changed?.Invoke ());
        Down.AddListener (() => Changed?.Invoke ());
    }

    public void AddListener (System.Action listener) {
        Changed += listener;
    }

}
