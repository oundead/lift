﻿using System.Collections.Generic;

public interface ILiftButtonsModel {
    IReadOnlyList<ILiftButtonModel> Floors { get; }
}
