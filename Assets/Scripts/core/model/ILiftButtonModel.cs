﻿public interface ILiftButtonModel {
    bool Toggled { get; set; }
    void AddListener (System.Action listener);
}
