﻿using System.Collections.Generic;

public interface IFloorListModel {
    IReadOnlyList<IFloorModel> Floors { get; }
    void AddListener (System.Action listener);
}
