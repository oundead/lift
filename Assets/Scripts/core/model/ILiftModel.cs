﻿public interface ILiftModel {
    float Position { get; }
    int? NextPosition { get; }
    bool StayingOnFloor { get; }
    ILiftButtonModel Stopped { get; }
    Directions Direction { get; set; }

    bool TryToSetNextPosition (int nextPosition);
    void UpdatePosition (float TimeDelta);
    void AddDirectionChangeListener (System.Action listener);
    void AddArrivedListener (System.Action arrived);
    void FireArrived ();
}
