﻿using UnityEngine;

public class DoorController : MonoBehaviour {
    [SerializeField]
    private Animator animator;

    private int Number;
    private IFloorModel Floor;
    private ILiftModel Lift;

    public void BindModel (ILiftModel lift, IFloorModel floor, int number) {
        if (Floor != null) {
            throw new System.Exception ("second bind");
        }
        Lift = lift;
        Floor = floor;
        Number = number;
        Lift.AddArrivedListener (LiftArrived);
    }

    private void LiftArrived () {
        if (Mathf.RoundToInt (Lift.Position) == Number) {
            Floor.DoorState = DoorStates.OPENED;
            animator.SetBool ("open", true);
        }
    }

    private void Closed () {
        Floor.DoorState = DoorStates.CLOSED;
        animator.SetBool ("open", false);
    }
}
