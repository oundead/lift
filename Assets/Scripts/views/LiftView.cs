﻿using UnityEngine;
using UnityEngine.UI;

public class LiftView : MonoBehaviour {
    [SerializeField]
    private Slider Control;
    private ILiftModel Model;

    public void BindModel (int floors, ILiftModel model) {
        if (Model != null) {
            throw new System.Exception ("second bind");
        }
        Model = model;
        Control.maxValue = floors;
    }

    public void Update () {
        if (Model == null) {
            return;
        }
        Control.value = Model.Position;
    }
}
