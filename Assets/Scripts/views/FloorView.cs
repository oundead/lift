﻿using UnityEngine;
using UnityEngine.UI;

public class FloorView : MonoBehaviour {
    [SerializeField]
    private LiftButtonControl UpControl;
    [SerializeField]
    private LiftButtonControl DownControl;
    [SerializeField]
    private Text FloorNumberLabel;
    [SerializeField]
    private DoorController DoorController;
    private IFloorModel Model;

    public void BindModel (int number, IFloorModel model, ILiftModel lift) {
        if (Model != null) {
            throw new System.Exception ("second bind");
        }
        FloorNumberLabel.text = number.ToString ();
        Model = model;
        UpControl.BindModel (model.Up);
        DownControl.BindModel (model.Down);
        DoorController.BindModel (lift, model, number);
    }
}
