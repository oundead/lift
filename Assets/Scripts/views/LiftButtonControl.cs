﻿using UnityEngine;
using UnityEngine.UI;

public class LiftButtonControl : MonoBehaviour {
    [SerializeField]
    private Toggle Control;
    private ILiftButtonModel Model;

    public void BindModel (ILiftButtonModel liftButtonModel) {
        if (Model != null) {
            throw new System.Exception ("Second bind");
        }
        Model = liftButtonModel;
        Control.isOn = Model.Toggled;
        Control.onValueChanged.AddListener ((arg0) => Model.Toggled = Control.isOn);
        Model.AddListener (() => Control.isOn = Model.Toggled);
    }
}
