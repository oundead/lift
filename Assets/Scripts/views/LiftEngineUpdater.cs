﻿using UnityEngine;

public class LiftEngineUpdater : MonoBehaviour {
    public LiftEngine Engine;

    private void FixedUpdate () {
        Engine?.Update (Time.fixedDeltaTime);
    }
}
