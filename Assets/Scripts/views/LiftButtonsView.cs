﻿using UnityEngine;
using UnityEngine.UI;

public class LiftButtonsView : MonoBehaviour {
    [SerializeField]
    private LiftButtonControl Prefab;
    private ILiftButtonsModel Model;

    public void BindModel (ILiftButtonsModel model) {
        if (Model != null) {
            throw new System.Exception ("second bind");
        }
        Model = model;
        for (int i = 0; i < model.Floors.Count; i++) {
            var instance = Instantiate (Prefab);
            instance.transform.SetParent (transform, false);
            instance.BindModel (Model.Floors [i]);
            instance.GetComponentInChildren<Text> ().text = i.ToString ();
        }
    }


}
