﻿using UnityEngine;
using UnityEngine.UI;

public class MainLiftPanelView : MonoBehaviour {
    [SerializeField]
    private LiftButtonControl StopButton;
    [SerializeField]
    private Text FloorText;
    [SerializeField]
    private Text DirectionText;

    private ILiftModel Model;

    private int LatestFloor;
    private Directions LatestDirection;

    public void BindModel (ILiftModel model) {
        if (Model != null) {
            throw new System.Exception ("second bind");
        }
        Model = model;
        StopButton.BindModel (Model.Stopped);
        LatestFloor = Mathf.RoundToInt (model.Position);
        LatestDirection = model.Direction;
        FloorText.text = LatestFloor.ToString ();
        DirectionText.text = LatestDirection.ToString ();
    }

    public void Update () {
        if (Model == null) {
            return;
        }
        if (LatestDirection != Model.Direction) {
            LatestDirection = Model.Direction;
            DirectionText.text = LatestDirection.ToString ();
        }
        if (LatestFloor != Mathf.RoundToInt (Model.Position)) {
            LatestFloor = Mathf.RoundToInt (Model.Position);
            FloorText.text = LatestFloor.ToString ();
        }
    }
}
