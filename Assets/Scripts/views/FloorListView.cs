﻿using UnityEngine;

public class FloorListView : MonoBehaviour {
    [SerializeField]
    private FloorView prefab;

    private IFloorListModel Model;

    public void BindModel (IFloorListModel model, ILiftModel lift) {
        if (Model != null) {
            throw new System.Exception ("second bind");
        }
        Model = model;
        for (int i = model.Floors.Count - 1; i >= 0; i--) {
            var instance = Instantiate (prefab);
            instance.transform.SetParent (transform, false);
            instance.BindModel (i, model.Floors [i], lift);
        }
    }

}
