﻿using UnityEngine;
using UnityEngine.UI;

public class Initializer : MonoBehaviour {

    public float LiftAcceleration = 1;
    public float LiftMaxSpeed = 1;

    public InputField CountField;

    public LiftEngineUpdater updater;
    public FloorListView floorListView;
    public LiftView liftView;
    public LiftButtonsView liftButtonsView;
    public MainLiftPanelView mainLiftPanelView;

    public void OnEnable () {
        transform.localScale = Vector3.one;
    }

    public void Click () {
        int count;
        try {
            count = int.Parse (CountField.text);
            if (count < 2) {
                return;
            }
        } catch {
            return;
        }
        Initialize (count);
        gameObject.SetActive (false);
    }

    void Initialize (int count) {
        var engine = new LiftEngine (count, LiftAcceleration, LiftMaxSpeed);

        updater.Engine = engine;
        floorListView.BindModel (engine.Floors, engine.Lift);
        liftView.BindModel (count, engine.Lift);
        liftButtonsView.BindModel (engine.LiftButtons);
        mainLiftPanelView.BindModel (engine.Lift);

        engine.Lift.AddDirectionChangeListener (() => Debug.Log ("DIR: " + engine.Lift.Direction));

        FixScrolls ();
    }

    void FixScrolls() {
        ScrollRect [] scrolls = new ScrollRect [] {
            liftButtonsView.GetComponentInParent<ScrollRect>(),
            floorListView.GetComponentInParent<ScrollRect>()
        };
        foreach (var s in scrolls) {
            s.enabled = false;
            s.enabled = true;
        }
    }
}
