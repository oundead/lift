﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    private Vector2? StartDragPosition;
    private RectTransform CanvasRT;
    private RectTransform PanelRT;

    public void Start () {
        Canvas canvas = GetComponentInParent<Canvas> ();
        if (canvas != null) {
            CanvasRT = canvas.transform as RectTransform;
            PanelRT = transform as RectTransform;
        }
    }

    public void OnBeginDrag (PointerEventData eventData) {
        PanelRT.SetAsLastSibling ();
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle (
            PanelRT, 
            eventData.position, 
            eventData.pressEventCamera,
            out Vector2 localPointerPosition
        )) {
            StartDragPosition = localPointerPosition;
        }
    }

    public void OnDrag (PointerEventData eventData) {
        if (PanelRT == null || StartDragPosition == null) {
            return;
        }
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
            CanvasRT, 
            eventData.position, 
            eventData.pressEventCamera, 
            out Vector2 localPointerPosition
        )) {
            PanelRT.localPosition = localPointerPosition - StartDragPosition.Value;         
        }
    }

    public void OnEndDrag (PointerEventData eventData) {
        StartDragPosition = null;
    }
}